import math
from datetime import datetime


# Декоратор
def timeit(func):
    def wrapper(*args):
        start = datetime.now()
        result = func(*args)
        print("Время для определения результата: " + str(datetime.now() - start))
        return result
    return wrapper


# Функция для определения простых чисел
@timeit
def isprime(object):
    res_simple = []
    for num in object:
        list_divider = []  # Список делителей
        for i in range(1, num + 1):
            if num % i == 0:  # Если число делится без остатка,
                            # заносим в список делителей
                list_divider.append(i)
        if len(list_divider) == 2:  # Если длина списка равна двум, значит
        # список содержит всего два делителя,
        # а значит число простое
            res_simple.append(num)
    return res_simple


# Функция возведение в квадрат списка чисел
@timeit
def exponentiation(number, sqr):
    results = []
    for i in number:
        if sqr > 1:
            results.append(i ** sqr)
        else:
            i = i ** sqr
    return results


# Функция для определения четных, нечетных элементов
def even(number, elements):
    res = []
    if elements == 2:
        for i in number:
            if i % 2 == 0:
                res.append(i)
    elif elements == 1:
        for i in number:
            if i % 2 == 1:
                res.append(i)
    elif elements == 3:
        res = isprime(number)

    else:
        res = "Некорректно указанно значение поиска"
    return res


if __name__ == "__main__":
# number_for_sqr = list(map(float, input("Введите Числа для возведения в квадрат, через пробел: ").split()))
    number_for_sqr = [2.0, 3.0, 5.0, 6.0]
# degree_of_number = int(input("Введите степень возведения: "))
    degree_of_number = 2


    print("Список числе в степени " + str(degree_of_number) + ":", exponentiation(number_for_sqr, degree_of_number))


# defining_complexity = list(map(int, input("Введите числа для определения сложности:").split()))
    defining_complexity = [2, 3, 5, 6, 7, 8]
# definitions_of_numbers = int(input("Для поиска нечетных чисел введите - 1  Для поиска четных чисел введите - 2 Для поиска простых чисел - 3:"))
    definitions_of_numbers = 3


    print(even(defining_complexity, definitions_of_numbers))
